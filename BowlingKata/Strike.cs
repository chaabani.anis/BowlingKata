﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Strike : Jeux
    {
        public Strike()
            : base(10, 0)
        {
            this.isStrike = true;
        }
    }
}
