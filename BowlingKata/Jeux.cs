﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Jeux
    {
        protected int coup1;
        protected int coup2;
        private bool isSpare;
        protected bool isStrike;
        private int numero;
        protected int bonus;

        protected Jeux jeuSuivant = null;

        public int Coup1 { get { return coup1; } }
        public int Coup2 { get { return coup2; } }

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public Jeux JeuSuivant
        {
            get { return jeuSuivant; }
            set { jeuSuivant = value; }
        }

        public bool IsSpare
        {
            get { return isSpare; }
            set { isSpare = value; }
        }

        public bool IsStrike
        {
            get { return isStrike; }
            set { isStrike = value; }
        }

        public int Bonus
        {
            get { return bonus; }
            set { bonus = value; }
        }

        public Jeux(int coup1, int coup2)
        {
            this.coup1 = coup1;
            this.coup2 = coup2;
        }

        public int TotalDuJeu
        {
            get
            {
                return coup1 + coup2 + bonus;
            }
        }
    }
}
