﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingKata
{
    public class Bowling
    {
        //TODO j'ai crée un projet console. si pas de programme principale le transformer en Assembly et supprimer le main
        public static void Main(){}

        private Jeux premierJeu = null;
        private Jeux dernierJeu = null;

        public Bowling()
        {
        }

        /// <summary>
        /// Permet de simuler le jeux d'une personne
        /// </summary>
        /// <param name="coup1">score de la premiere lancé</param>
        /// <param name="coup2">score de la deuxième lancé</param>
        public void LancerJeux(int coup1, int coup2)
        {
            AjouterJeux(new Jeux(coup1, coup2));
        }

        /// <summary>
        /// Mon jeux est un Spare
        /// </summary>
        /// <param name="coup1">score de la premiere lancé</param>
        /// <param name="coup2">score de la deuxième lancé</param>
        public void Spare(int coup1, int coup2) 
        {
            Jeux jeu = new Jeux(coup1, coup2);
            jeu.IsSpare = true;
            AjouterJeux(jeu);
        }

        /// <summary>
        /// Il s'agit d'un Strike, je crée mon objet
        /// </summary>
        public void Strike()
        {
            Strike strike = new Strike();
            AjouterJeux(strike);
        }

        /// <summary>
        /// Il s'agit du 1eme coup, je rajoute mon bonus
        /// </summary>
        /// <param name="coup1">score du premier coup</param>
        /// <param name="coup2">score du deuxième coup</param>
        /// <param name="coup3">score du bonus</param>
        public void DixiemeJeu(int coup1, int coup2, int coup3)
        {
            Jeux jeu = new Jeux(coup1, coup2);
            jeu.Bonus = coup3;
            AjouterJeux(jeu);
        }

        /// <summary>
        /// J'ajoute un jeux à ma liste
        /// </summary>
        /// <param name="jeu">le jeu a ajouter</param>
        private void AjouterJeux(Jeux jeu)
        {
            if (premierJeu == null)
            {
                jeu.Numero = 1;
                premierJeu = jeu;
                dernierJeu = jeu;
            }
            else
            {
                if (dernierJeu.Numero == 10)
                {
                    throw new ArgumentOutOfRangeException("Vous ne pouvez pas jouer plus de 10 fois");
                }
                jeu.Numero = dernierJeu.Numero + 1;
                dernierJeu.JeuSuivant= jeu;
                dernierJeu = jeu;
            }
        }

        /// <summary>
        /// Calcule du score
        /// </summary>
        public int Score
        {
            get
            {
                int score = 0;
                Jeux jeuCourant = premierJeu;
                while(jeuCourant != null)
                {
                    score += jeuCourant.TotalDuJeu;
                    if(jeuCourant.IsSpare)
                    {
                        score += jeuCourant.JeuSuivant.Coup1;
                    }
                    else if (jeuCourant.IsStrike)
                    {
                        score += jeuCourant.JeuSuivant.Coup1;
                        if(jeuCourant.JeuSuivant.IsStrike)
                        {
                            score += jeuCourant.JeuSuivant.JeuSuivant.Coup1;
 
                        }
                        else
                        {
                            score += jeuCourant.JeuSuivant.Coup2;
                        }
                    }

                    jeuCourant = jeuCourant.JeuSuivant;
                }

                return score;
            }   
        }
    }
}
