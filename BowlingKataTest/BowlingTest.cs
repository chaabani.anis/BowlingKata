﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BowlingKata;
namespace BowlingKataTest
{
    [TestClass]
    public class BowlingTest
    {
        Bowling jeux = null;

        [TestInitialize]
        public void SetUp()
        {
            jeux = new Bowling();
        }

        [TestMethod]
        public void AucunPointMarque()
        {
            LancerJeux(0, 0, 10);
            Assert.AreEqual(0, jeux.Score, "Aucun point marqué");
        }

        [TestMethod]
        public void DeuxLancee3Puis2()
        {
            LancerJeux(3, 2, 10);
            Assert.AreEqual(50, jeux.Score, "3 points première lancé et 2 deuxième lancé, Score = 50");
        }

        [TestMethod]
        public void PremierJeuxSpareReste2Puis2AChaqueLancee()
        {
            jeux.Spare(2, 8);
            LancerJeux(3, 2, 9);
            Assert.AreEqual(58, jeux.Score, "Le premier jeux Spare puis deux points marqué à chaque lancé, Score = 58");
        }

        [TestMethod]
        public void LesDeuxPremiersJeuxSpareReste2Puis2AchaqueLancee()
        {
            jeux.Spare(2, 8);
            jeux.Spare(2, 8);
            LancerJeux(3, 2, 8);
            Assert.AreEqual(65, jeux.Score, "Les deux premiers jeux Spare puis deux points à chaque lancé, Score = 65");
        }

        [TestMethod]
        public void PremierJeuxStrikePuis2AChaqueLancee()
        {
            jeux.Strike();
            LancerJeux(3, 2, 9);
            Assert.AreEqual(60, jeux.Score, "Le premier jeux Strike puis deux points à chaque lancé, Score = 61");
        }

        [TestMethod]
        public void LesDeuxPremiersJeuxStrikePuis2AChaqueLancee()
        {
            jeux.Strike();
            jeux.Strike();
            LancerJeux(3, 2, 8);
            Assert.AreEqual(78, jeux.Score, "Les deux premiers jeux sont des Strike puis deux points à chaque lancé, Score = 78");
        }

        [TestMethod]
        public void TousLesCoupsSontDesStrikes()
        {
            for (int i = 1; i <= 9; i++)
            {
                jeux.Strike();
            }
            jeux.DixiemeJeu(10, 10, 10);
            Assert.AreEqual(300, jeux.Score, "Tous les coups sont des Strike, Score = 300");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Vous ne pouvez pas jouer plus de 10 fois")]
        public void PlusDe10JeuxImpossible()
        {
            LancerJeux(3, 3, 12);
        }

        private void LancerJeux(int coup1, int coup2, int NbJeux)
        {
            for (int frame = 1; frame <= NbJeux; frame++)
            {
                jeux.LancerJeux(coup1, coup2);
            }
        }
    }
}
